#include <iostream>
#include <lemon/list_graph.h>

using namespace std;
using namespace lemon;

int main() {
	int a = 2211;
	std::cout << a << "\n";

	lemon::ListDigraph g;
	lemon::ListDigraph::Node v = g.addNode();
	lemon::ListDigraph::Node w = g.addNode();
	lemon::ListDigraph::Arc e = g.addArc(v,w);

	std::cout << "nodes: " << countNodes(g) << "\n";

	return 0;
}
